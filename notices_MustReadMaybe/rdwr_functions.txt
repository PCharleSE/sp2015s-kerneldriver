########################################################################
Macro functions
########################################################################
get_user(x, ptr) : user -> kernel

ptr이 가리키는 사용자 공간의 메모리에서 x 변수의 크기만큼 read

put_user(x, ptr) : kernel -> user

ptr이 가리키는 사용자 공간의 메모리에 x를 write

########################################################################
asm/uaccess.h functions
########################################################################
copy_from_user(void * to, const void __user * from, unsigned long n)
	user -> kernel
from이 가리키는 사용자 공간의 메모리에서 n만큼 read
to가 가리키는 커널 메모리에 n만큼 write.
ret_val < 0 : Error

copy_to_user(void __user * to, const void * from, unsigned long n)
	user -> kernel
from이 가리키는 커널 메모리에서 n만큼 read
to가 가리키는 사용자 공간의 메모리에 n만큼 write.
ret_val < 0 : Error

########################################################################
device functions
########################################################################
device_read(filp, buffer, length, offset) : kernel -> user

디바이스 파일의 버퍼를 읽어서 사용자 공간으로 전송

device_write(filp, buffer, length, offset) : user -> kernel

디바이스 파일에 사용자 공간의 데이터를 전송
