#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "char_dev.h"

static void msg(const char * str) {
	static int cnt;
	printf(">> %s...: %d\n", str, cnt++);
}

void print_node_line(List list) {
	printf("%s(%d)\n", list->name, list->pid);
}

void pretty_print(List arr) {
	int i, j, k;
	for (i = 0; arr->pid >= 0; i++)
		arr++;
	arr--;
	for (j = 0; i > 0; i--, j++) {
		for (k = 0; k < j; k++)
			printf("  ");
		if (j != 0)
			printf("\\-");
		print_node_line(arr--);
	}
}

ioctl_get_msg(int fd) {
	int ret_val;
	List arr = (List) malloc(sizeof(struct node) * BUFF_LEN);

	ret_val = ioctl(fd, IOCTL_SET_MSG, arr);
	if (ret_val < 0) {
		printf("ioctl_set_msg faild: %d\n", ret_val);
		exit(-1);
	}

	ret_val = ioctl(fd, IOCTL_GET_MSG, 0);
	if (ret_val < 0) {
		printf("ioctl_get_msg faild: %d\n", ret_val);
		exit(-1);
	}
	pretty_print(arr);
}

int main() {
	int fd, ret;
	char prefix[BUFF_LEN] = "/dev/";
	strcat(prefix, DEVICE_FILE_NAME);

	fd = open(prefix, O_RDONLY);
	if (fd < 0) {
		printf("Faild to open device file: %s", DEVICE_FILE_NAME);
		exit(-1);
	}

	ioctl_get_msg(fd);
	close(fd);
}
