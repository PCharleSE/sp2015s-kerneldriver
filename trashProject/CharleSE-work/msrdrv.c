#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include "msrdrv.h"

#ifdef _MG_DEBUG
#define dprintk(args...) printk(args);
#else
#define dprintk(args...)
#endif

MODULE_LICENSE("Dual BSD/GPL");

int Dev_Open = 0;

static int device_open(struct inode* i, struct file* f)
{
    if (Dev_Open) return -EBUSY;
    Dev_Open++;

    try_module_get(THIS_MODULE);

    return 0;
}

static int device_release(struct inode* i, struct file* f)
{
    Dev_Open--;
    module_put(THIS_MODULE);
    return 0;
}

static ssize_t device_read(struct file *f, char *b, size_t c, loff_t *o)
{
    return 0;
}

static ssize_t device_write(struct file *f, const char *b, size_t c, loff_t *o)
{
    return 0;
}

int device_ioctl(struct file *f, unsigned int ioctl_num, unsigned long ioctl_param);

struct file_operations msrdrv_fops = {
    .read = device_read,
    .write = device_write,
    .unlocked_ioctl = device_ioctl,
    .compat_ioctl = NULL,
    .open = device_open,
    .release = device_release,
};

static long long read_msr(unsigned int ecx) // Read
{
    unsigned int edx = 0, eax = 0;
    unsigned long long result = 0;
    __asm__ __volatile("rdmsr" : "=a"(eax), "=d"(edx) : "c"(ecx));
    result = eax | (unsigned long long)edx << 0x20;
    dprintk(KERN_ALERT "Module msrdrv: Read 0x%016llx (0x%08x:0x%08x) from MSR 0x%08x\n", result, edx, eax, ecx)
    return result;
}

static void write_msr(int ecx, unsigned int eax, unsigned int edx) // Write
{
    dprintk(KERN_ALERT "Module msrdrv: Writing 0x%08x:0x%08x to MSR 0x%04x\n", edx, eax, ecx)
    __asm__ __volatile__("wrmsr" : : "c"(ecx), "a"(eax), "d"(edx));
}

static long long read_tsc(void) // Time Spamp Counter read
{
    unsigned eax, edx;
    long long result;
    __asm__ __volatile__("rdtsc" : "=a"(eax), "=d"(edx));
    result = eax | (unsigned long long)edx << 0x20;
    dprintk(KERN_ALERT "Module msrdrv: Read 0x%016llx (0x%08x:0x%08x) from TSC\n", result, edx, eax)
    return result;
}

int device_ioctl(struct file *f, unsigned int ioctl_num, unsigned long ioctl_param)
{
    struct MsrInOut *msrops;
    int i;
    if(ioctl_num != IOCTL_MSR_CMDS){ // If attack occurs
        return 0;
    }
    msrops = (struct MsrInOut *)ioctl_param;
    for(i=0; i<=MSR_VEC_LIMIT; i++, msrops++){
        long long msrvalue;
        switch (msrops->op){
        case MSR_NOP:
            dprintk(KERN_ALERT "Module " DEV_NAME ": seen MSR_NOP command\n")
            break;
        case MSR_STOP:
            dprintk(KERN_ALERT "Module " DEV_NAME ": seen MSR_STOP command\n")
            goto label_end;
        case MSR_READ:
            dprintk(KERN_ALERT "Module " DEV_NAME ": seen MSR_READ command\n")
            // msrops->value = read_msr(msrops->ecx);
            msrvalue = read_msr(msrops->ecx);
            copy_to_user(&(msrops->value), &msrvalue, sizeof(long long));
            break;
        case MSR_WRITE:
            dprintk(KERN_ALERT "Module " DEV_NAME ": seen MSR_WRITE command\n")
            write_msr(msrops->ecx, msrops->eax, msrops->edx);
            break;
        case MSR_RDTSC:
            dprintk(KERN_ALERT "Module " DEV_NAME ": seen MSR_RDTSC command\n")
            // msrops->value = read_tsc();
            msrvalue = read_tsc();
            copy_to_user(&(msrops->value), &msrvalue, sizeof(long long));
            break;
        default:
            dprintk(KERN_ALERT "Module " DEV_NAME ": Unknown option 0x%x\n", msrops->op)
            return 1;
        }
    }
    label_end:

    return 0;
}

static int msrdrv_init(void)
{
    int ret_val = register_chrdev(DEV_MAJOR, DEV_NAME, &msrdrv_fops);
    if (ret_val < 0) {
        printk(KERN_ALERT "registering the character device failed with %d\n", ret_val);
        return ret_val;
    }
    printk(KERN_ALERT "Module " DEV_NAME "loaded\n");
    return 0;
}

static void msrdrv_exit(void)
{
    unregister_chrdev(DEV_MAJOR, DEV_NAME);
    printk(KERN_ALERT "Module " DEV_NAME "unloaded\n");
}

module_init(msrdrv_init);
module_exit(msrdrv_exit);
