#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/cdev.h>

#include "msrdrv.h"

MODULE_LICENSE("Dual BSD/GPL");

/* Declaration of functions */
void device_exit(void);
int device_init(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char __user *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char __user *, size_t, loff_t *);
int device_ioctl(struct file *, unsigned int, unsigned long);

static long long read_msr(unsigned int);
static void write_msr(int, unsigned int, unsigned int);
static long long read_tsc(void);

/* Declaration of the init and exit routines */
module_init(device_init);
module_exit(device_exit);

struct file_operations msrdrv_fops = {
	.owner = THIS_MODULE,
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release,
	.unlocked_ioctl = device_ioctl,
	.compat_ioctl = NULL,
};

int Device_Open = 0;

int device_init() {
	int ret_val = register_chrdev(MAJOR_NUM, DEV_NAME, &msrdrv_fops);

	if (ret_val < 0) {
		printk(KERN_ALERT " ==== register failed: %d ====\n", ret_val);
		return ret_val;
	}

	printk(KERN_NOTICE "==== init module ====\n");
	return 0;
}

void device_exit() {
	unregister_chrdev(MAJOR_NUM, DEV_NAME);
	printk(KERN_NOTICE "==== exit module ====\n");
}

static int device_open(struct inode * i, struct file * f)
{
	if (Device_Open)
		return -EBUSY;

	Device_Open++;

	try_module_get(THIS_MODULE);
	return 0;
}

static int device_release(struct inode * i, struct file * f)
{
	Device_Open--;

	module_put(THIS_MODULE);
	return 0;
}

static ssize_t device_read(
	struct file * f,
	char __user * b,
	size_t c,
	loff_t * o)
{
	// Don't use this function
	return 0;
}

static ssize_t device_write(
	struct file * f,
	const char __user * b,
	size_t c,
	loff_t * o)
{
	// Don't use this function
	return 0;
}

dev_t msrdrv_dev;
struct cdev * msrdrv_cdev;

static long long read_msr(unsigned int ecx) {
	unsigned int edx = 0, eax = 0;
	unsigned long long result = 0;
	__asm__ __volatile__("rdmsr" : "=a"(eax), "=d"(edx) : "c"(ecx));
	result = eax | (unsigned long long) edx << 0x20;

	printk(KERN_INFO "Module msrdrv: Read 0x%016llx (0x%08x :0x%08x) from MSR 0x%08x\n",
		result, edx, eax, ecx);

	return result;
}

static void write_msr(int ecx, unsigned int eax, unsigned int edx) {
	printk(KERN_INFO "Module msrdrv: Writing 0x%08x :0x%08x to MSR 0x%04x\n",
		edx, eax, ecx);

	__asm__ __volatile__("wrmsr" : : "c"(ecx), "a"(eax), "d"(edx));
}

static long long read_tsc() {
	unsigned eax, edx;
	long long result;
	__asm__ __volatile__("rdtsc" : "=a"(eax), "=d"(edx));
	result = eax | (unsigned long long) edx << 0x20;

	printk(KERN_INFO "Module msrdrv: Read 0x%016llx (0x%08x :0x%08x) from TSC\n",
		result, edx, eax);

	return result;
}

int device_ioctl(
	struct file * f,
	unsigned int cmd,
	unsigned long argp)
{
	struct MsrInOut * msrops;
	int i;
	long long read_val;

	if (cmd != IOCTL_MSR_CMDS)
		return 0;

	msrops = (struct MsrInOut *) argp;
	for(i = 0; i <= MSR_VEC_LIMIT; i++, msrops++) {
		switch (msrops->op) {
			case MSR_NOP:
				printk(KERN_ALERT "Module msrdrv: seen MSR_NOP command\n");
				break;
			case MSR_STOP:
				printk(KERN_ALERT "Module msrdrv: seen MSR_STOP command\n");
				goto Done;
			case MSR_READ:
				printk(KERN_ALERT "Module msrdrv: seen MSR_READ command\n");
				read_val = read_msr(msrops->ecx);
				copy_to_user( &(msrops->value),  &read_val, sizeof(long long));
				break;
			case MSR_WRITE:
				printk(KERN_ALERT "Module msrdrv: seen MSR_WRITE command\n");
				write_msr(msrops->ecx, msrops->eax, msrops->edx);
				break;
			case MSR_RDTSC:
				printk(KERN_ALERT "Module msrdrv: seen MSR_RDTSC command\n");
				read_val = read_tsc();
				copy_to_user( &(msrops->value), &read_val, sizeof(long long));
				break;
			default:
				printk(KERN_ALERT "Module msrdrv: Unknown option 0x%x\n", msrops->op);
				return 1;
		}
	}
Done:
	return 0;
}
