#include "msrdrv.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static int loadDriver() {
	char route[64] = "/dev/";
	strcat(route, DEV_NAME);

	int fd = open(route, O_RDWR);
	if (fd < 0) {
		printf("Faild to open %s\n", route);
		exit(-1);
	}
	return fd;
}

static void closeDriver(int fd) {
	int ret_val = close(fd);
	if (ret_val < 0) {
		printf("Faild to close fd:%d\n", fd);
		exit(-1);
	}
}

int main() {
	int fd;
	struct MsrInOut msr_start[] = {
		{ MSR_WRITE, 0x38f, 0x00, 0x00 },
		{ MSR_WRITE, 0xc1, 0x00, 0x00 },
<<<<<<< HEAD:ProjectFolder/khm-workspace/usr.c
		{ MSR_WRITE, 0x186, 0x00410180, 0x00 },
=======
		{ MSR_WRITE, 0xc2, 0x00, 0x00 },
		{ MSR_WRITE, 0xc3, 0x00, 0x00 },
		{ MSR_WRITE, 0xc4, 0x00, 0x00 },
		{ MSR_WRITE, 0x309, 0x00, 0x00 },
		{ MSR_WRITE, 0x30a, 0x00, 0x00 },
		{ MSR_WRITE, 0x30b, 0x00, 0x00 },
		{ MSR_WRITE, 0x186, 0x004101c2, 0x00 },
		{ MSR_WRITE, 0x187, 0x0041010e, 0x00 },
		{ MSR_WRITE, 0x188, 0x01c1010e, 0x00 },
		{ MSR_WRITE, 0x189, 0x004101a2, 0x00 },
>>>>>>> CharleSE0414-2:trashProject/khm-workspace/usr.c
		{ MSR_WRITE, 0x38d, 0x222, 0x00 },
		{ MSR_WRITE, 0x38f, 0x0f, 0x07 },
		{ MSR_STOP, 0x00, 0x00 }
	};

	struct MsrInOut msr_stop[] = {
		{ MSR_WRITE, 0x38f, 0x00, 0x00 },
		{ MSR_WRITE, 0x38d, 0x00, 0x00 },
		{ MSR_READ, 0xc1, 0x00 },
		{ MSR_RDTSC, 0x00, 0x00 },
		{ MSR_STOP, 0x00, 0x00 }
	};

	fd = loadDriver();
	ioctl(fd, IOCTL_MSR_CMDS, (long long) msr_start);
	ioctl(fd, IOCTL_MSR_CMDS, (long long) msr_stop);
	printf("-----------------------------\n");
	printf("instr fetch counts:		%7lld\n", msr_stop[2].value);
	printf("time stamp counts:		%7lld\n", msr_stop[3].value);
	printf("-----------------------------\n");
	closeDriver(fd);
	return 0;
}

