#ifndef _MG_MSRDRV_H
#define _MG_MSRDRV_H

#include <linux/ioctl.h>
#include <linux/types.h>

#define DEV_NAME "msrdrv"
#define MAJOR_NUM 222
#define MINOR_NUM 0

#define MSR_VEC_LIMIT 32

enum MsrOperation {
	MSR_NOP = 0,
	MSR_READ = 1,
	MSR_WRITE = 2,
	MSR_STOP = 3,
	MSR_RDTSC = 4
};

struct MsrInOut {
	unsigned int op;							// Msr Operation
	unsigned int ecx;							// Msr Identifier
	union {
		struct {
			unsigned int eax;					// Low dword
			unsigned int edx;					// High dword
		};
		unsigned long long value;		// qword
	};
};

#define IOCTL_MSR_CMDS _IOWR(MAJOR_NUM, 1, struct MsrInOut *)

#endif
