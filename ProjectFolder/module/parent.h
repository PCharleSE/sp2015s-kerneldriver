#ifndef PARENT_H
#define PARENT_H

#include <linux/ioctl.h>

#define SUCCESS 0
#define MAJOR_NUM 50
#define BUFF_LEN 256
#define DEVICE_FILE_NAME "parent"

typedef struct node {
	char name[BUFF_LEN];
	int pid;
} Node;
typedef Node * List;

#define IOCTL_GET_MSG _IOR(MAJOR_NUM, 0, List)
#define IOCTL_SET_MSG _IOW(MAJOR_NUM, 1, List)

static void mystrcpy(const char * src, char * des) {
	while (*src) *des++ = *src++; // move process name to our structure.
	*des = '\0'; // end of string is NULL char.
}

#endif
