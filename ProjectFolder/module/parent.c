#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include "parent.h"

MODULE_LICENSE("Dual BSD/GPL");

/* Declaration of functions */
void device_exit(void);
int device_init(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char __user *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char __user *, size_t, loff_t *);
int device_ioctl(struct file *, unsigned int, unsigned long);

/* Declaration of the init and exit routines */
module_init(device_init);
module_exit(device_exit);

static int Device_Open = 0;
static List Buff_Ptr;

struct file_operations Fops = {
	.read = device_read,
	.write = device_write,
	.unlocked_ioctl = device_ioctl,
	.open = device_open,
	.release = device_release,
};

int device_init(void) {
	int ret_val = register_chrdev(MAJOR_NUM, DEVICE_FILE_NAME, &Fops);
  printk(KERN_NOTICE "initing module");

	if (ret_val < 0) {
		printk(KERN_ALERT "registering the character device failed with %d\n", ret_val);
		return ret_val;
	}
  return SUCCESS;
}

void device_exit(void) {
	unregister_chrdev(MAJOR_NUM, DEVICE_FILE_NAME);
  printk(KERN_NOTICE "exiting module");
}

static int
device_open(struct inode * inode, struct file * filp) {
	if (Device_Open)
		return -EBUSY;

	Device_Open++;

	try_module_get(THIS_MODULE);
	return SUCCESS;
}

static int
device_release(struct inode * inode, struct file * file) {
	Device_Open--;

	module_put(THIS_MODULE);
	return SUCCESS;
}

static ssize_t device_read(
	struct file * file,
	char __user * buffer,
	size_t length,
	loff_t * offset)
    // cannot access from user. This function will be called by
    // only this kernel driver.
    // We used this predefined function to make proper module.
{
	int i, ret_val;
	struct task_struct * task = current;
	Node tmp_struct;
    // Below code is same as in do_while loop, but this is required to print caller.
	mystrcpy(task->comm, tmp_struct.name);
    // move process name from task_struct to our struct only using pointer
	tmp_struct.pid = task->pid; // move PID
	ret_val = copy_to_user(Buff_Ptr++, &tmp_struct, (unsigned long) sizeof(Node));
	do {
		task = task->parent;
        // By this code, this loop will send structure from caller's parent to root process.
		mystrcpy(task->comm, tmp_struct.name);
        // move process name from task_struct to our struct only using pointer
		tmp_struct.pid = task->pid; // move PID
		ret_val = copy_to_user(Buff_Ptr++, &tmp_struct, (unsigned long) sizeof(Node));
        // copy data of tmp_struct to Buff_Ptr.
        // Buffer_Ptr points user buffer,
        // therefore user buffer will contain struct successfully
        // ret_val : check copy_to_user is valid or not
	} while (task->pid != 0 && --length); // if buffer length is too short, error

		for (i = 0; i < BUFF_LEN; i++)
			tmp_struct.name[i] = '\0'; // make tmp_struct empty
		tmp_struct.pid = -1;
        // Since real process has unsigned pid,
        // Make virtual process with pid = -1
		ret_val = copy_to_user(Buff_Ptr, &tmp_struct, (unsigned long) sizeof(Node));

	return SUCCESS;
}

static ssize_t device_write(
	struct file * filp,
	const char __user * buffer,
	size_t length,
	loff_t * offset)
    // By same reason of device_write
{
	Buff_Ptr = (List) buffer;
    // Initialize Buff_ptr as user buffer,
    // then Buff_Ptr will contain Parent Process Tree
	return SUCCESS;
}

int device_ioctl(
	struct file * file,
	unsigned int cmd,
	unsigned long argp)
{
	switch (cmd) {
	case IOCTL_SET_MSG:
		device_write(file, (char *) argp, 0, 0);
        // Initialize Buff_Ptr
		break;

	case IOCTL_GET_MSG:
		device_read(file, (char *) argp, BUFF_LEN-1, 0);
        // Get each process' name and pid as user-defined buffer structure
		break;
	}
	return SUCCESS;
}
