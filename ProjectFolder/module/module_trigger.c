#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "parent.h"

void print_node_line(List list) {
	printf("%s(%d)\n", list->name, list->pid);
}

void pretty_print(List arr) {
	int i, j, k;
	for (i = 0; arr->pid >= 0; i++)
		arr++;
	arr--; // This initialization will find information of root process
	for (j = 0; i > 0; i--, j++) { // i means # of process
		for (k = 0; k < j; k++)
			printf("  ");
		if (j != 0)
			printf("\\-");
        // above for loop makes empty space to make output functional
		print_node_line(arr--);
        // print process name and id,
        // and move to child process information.
        // arr-- is possible since (arr+1) points to parent process
	}
}

ioctl_get_msg(int fd) {
	int ret_val;
	List arr = (List) malloc(sizeof(struct node) * BUFF_LEN);

	ret_val = ioctl(fd, IOCTL_SET_MSG, arr);
    // Buff_Ptr(which will contain our structure)
    // will point user buffer structure
	if (ret_val < 0) {
		printf("ioctl_set_msg failed: %d\n", ret_val);
		exit(-1);
	}

	ret_val = ioctl(fd, IOCTL_GET_MSG, 0);
    // set user buffer.
	if (ret_val < 0) {
		printf("ioctl_get_msg failed: %d\n", ret_val);
		exit(-1);
	}
	pretty_print(arr);
}

int main() {
	int fd, ret;
	char prefix[BUFF_LEN] = "/dev/";
	strcat(prefix, DEVICE_FILE_NAME);
    // DEVICE_FILE_NAME is name of device driver.
    // prefix will contain absolute path to insert module.

	fd = open(prefix, O_RDONLY);
	if (fd < 0) {
		printf("Faild to open device file: %s", DEVICE_FILE_NAME);
		exit(-1);
	}

	ioctl_get_msg(fd); // will get and print Parent Process Tree
	close(fd);
}
