#include "msrdrv.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
static int loadDriver(){
    int fd = open("/dev/" DEV_NAME, O_RDWR);
    if(fd < 0){
        perror("Failed to open /dev/" DEV_NAME);
    }
    return fd;
}

static void closeDriver(int fd){
    int e = close(fd);
    if (e < 0){
        perror("Failed to close /dev/" DEV_NAME);
    }
}
int main(){
    int fd;
    struct MsrInOut msr_start[] = {
        { MSR_WRITE, 0x38f, 0x00, 0x00 },
        { MSR_WRITE, 0xc1, 0x00, 0x00 }, // L1 Instruction fetch hit counter init
       // { MSR_WRITE, 0x10, 0x00, 0x00 }, // time stamp init
        { MSR_WRITE, 0x186, 0x00410180, 0x00 }, // L1 Instruction fetch hit
        { MSR_WRITE, 0x38d, 0x222, 0x00 },
        { MSR_WRITE, 0x38f, 0x0f, 0x07 },
        { MSR_STOP, 0x00, 0x00 }
    };

    struct MsrInOut msr_stop[] = {
        { MSR_WRITE, 0x38f, 0x00, 0x00 },
        { MSR_WRITE, 0x38d, 0x00, 0x00 },
        { MSR_READ, 0xc1, 0x00 }, // L1 Instruction fetch hit counter write
        { MSR_RDTSC, 0x10, 0x00 }, // time stamp
        { MSR_STOP, 0x00, 0x00 },
    };
    fd = loadDriver();
    int i, s=20, data[20] = {7915, 939875, 657257, 452300, 337257, 800936,
        151665, 687304, 528477, 352729, 744109, 656966, 444444, 932702,
        416667, 766689, 999999, 566226, 360689, 111111};
    printf("%d integer which will be printed below will be sorted after PMU started.\n", s);
    ioctl(fd, IOCTL_MSR_CMDS, (long long)msr_start);
    printf("PMU Reseted and started.\n");
    for(i=0; i<s; i++)
    {
        printf("%d ", data[i]);
    }
    printf("\nThis program will sort above integers.\n");
    int j, temp;
    for(i=0; i<s-1; i++){
        for(j=i+1; j<s; j++){
            if(data[i] < data[j]){
                temp = data[i];
                data[i] = data[j];
                data[j] = temp;
            }
        }
        printf("%d ", data[i]);
    }
    printf("%d\n", data[s-1]);
    ioctl(fd, IOCTL_MSR_CMDS, (long long)msr_stop);
    printf("PMC stopped. This program will read PMU counter.\n");
    printf("L1 instruction fetch hit: %05lld\n", msr_stop[2].value);
    printf("Time Stamp: %016lld(0x%016llx)\n", msr_stop[3].value, msr_stop[3].value);
    closeDriver(fd);
    return 0;
}
